
#!/bin/sh
#
### BEGIN INIT INFO
# Provides:          relay-service
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: relay-service
# Description:       relay-service
### END INIT INFO
#
# Get function from functions library
# Start the service
start() {

        sudo node /opt/iot_gateway/retter/relay-thing.js --configuration-file=/etc/opt/iot_gateway/config/config.json &

        echo
}
# Restart the service
stop() {

        killproc relay-thing.js

        echo
}
### main logic ###
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  status)
        ;;
  restart|reload|condrestart)
        stop
        start
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart|reload|status}"
        exit 1
esac
exit 0
