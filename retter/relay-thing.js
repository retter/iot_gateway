/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

//node.js deps

//npm deps

//app deps
const thingShadow = require('..').thingShadow;
const cmdLineProcess = require('./lib/cmdline');
const Gpio = require('./lib/gpio.js');
const pigpio = require('rpi-gpio');
const utils = require('./lib/utils.js');
const winston = require('winston');

const exec = require('child_process').exec;
const networkConfigUpdater = require("./lib/network-config-updater");

var reconnectTryCount = 0;

// Configura logging
winston.add(winston.transports.File, {
    filename: '/var/log/relay-thing.log',
    maxsize: 1024*1024*20, // MAX log file size is 20MB - Logs will be transferred to CloudWatch anyways
    maxFiles: 1
});
console.log = winston.info;
console.error = winston.error;


var pinStatus = {
    "r1": 1,
    "r1t": 0,
    "r2": 1,
    "r2t": 0,
    "gpioError": 0
};

var networkConfigUtil = networkConfigUpdater();

function processTest(args) {

    winston.info('Starting with arguments: ' + JSON.stringify(args));

    process.on('uncaughtException', function(err) {
        winston.info('Caught exception: ' + err);
        reboot();
    });

    function reboot() {
        exec('sudo reboot');
    }

    // var pinno = 23;
    // pigpio.setup(pinno, pigpio.DIR_IN, readInput);
    // function readInput() {
    //     pigpio.read(pinno, function(err, value) {
    //         console.log('The value is ' + value);
    //     });
    // }
    //
    // setInterval(function(){
    //     pigpio.read(pinno, function(err, value) {
    //         console.log('The value is ' + value);
    //     });
    // }, 1000);


    //
    // Instantiate the thing shadow class.
    //
    const thingShadows = thingShadow({
        keyPath: args.privateKey,
        certPath: args.clientCert,
        caPath: args.caCert,
        clientId: args.clientId,
        region: args.region,
        baseReconnectTimeMs: args.baseReconnectTimeMs,
        keepalive: args.keepAlive,
        protocol: args.Protocol,
        port: args.Port,
        host: args.Host,
        debug: args.Debug
    });

    var thingList = args.things;

    var thingName = args.thingName;

    var thingStates = {};

    var gpio = new Gpio();

    console.log("args.deviceType: " + args.deviceType);
    //
    // Operation timeout in milliseconds
    //
    const operationTimeout = 10000;

    var currentTimeout = null;


    // Setup gpio pins
    if(args.deviceType == 'orangepilite') {
        gpio.setupPin(0);
        gpio.setupPin(1);
    } else {
        gpio.setupPin(23);
        gpio.setupPin(24);
    }



    var stack = [];

    function genericOperation(thingName, operation, state) {

        var clientToken = thingShadows[operation](thingName, state);

        if (clientToken === null) {
            //
            // The thing shadow operation can't be performed because another one
            // is pending; if no other operation is pending, reschedule it after an
            // interval which is greater than the thing shadow operation timeout.
            //
            if (currentTimeout !== null) {
                winston.info('operation in progress, scheduling retry...');
                currentTimeout = setTimeout(
                    function () {
                        genericOperation(operation, state);
                    },
                    operationTimeout * 2);
            }
        } else {
            //
            // Save the client token so that we know when the operation completes.
            //
            stack.push(clientToken);
        }
    }

    function pinNumberForRelay(relayNumber) {
        if(args.deviceType == 'orangepilite') {
            switch (relayNumber) {
                case 1:
                    return 0
                case 2:
                    return 1
            }
        } else {
            switch (relayNumber) {
                case 1:
                    return 23
                case 2:
                    return 24
            }
        }
    }

    // Register to all thing shadows in config file

    thingShadows.register(thingName, {
        ignoreDeltas: false,
        operationTimeout: operationTimeout
    });
    thingShadows.subscribe("ping/" + thingName, null, function(data){
        winston.info('Ping channel subscribed: ' + "ping/" + thingName);
    });
    thingShadows.subscribe("command/" + thingName, null, function(data){
        winston.info('Command channel subscribed: ' + "command/" + thingName);
    });

    // Update related
    thingShadows.subscribe("swupdate", null, function(data){
        winston.info('public swupdate channel subscribed: ' + "ping/" + thingName);
    });
    thingShadows.subscribe("swupdate/" + thingName, null, function(data){
        winston.info('swupdate channel subscribed: ' + "command/" + thingName);
    });

    thingShadows.subscribe("swupdateSystem", null, function(data){
        winston.info('public swupdateSystem channel subscribed: ' + "ping/" + thingName);
    });
    thingShadows.subscribe("swupdateSystem/" + thingName, null, function(data){
        winston.info('swupdateSystem channel subscribed: ' + "command/" + thingName);
    });
    // End of update related

    function rebootIfRaspberry() {
        console.log("reboot called");
        if (process.platform.indexOf("darwin") == -1) {
            console.log("Rebooting");
            exec('sudo reboot');
        }
    }

    function updateSW() {

        console.log("update command received");

        // Return if macos
        if (process.platform.indexOf("darwin") != -1) {
            console.log('not updating in darwin environment, returning from updateSW()');
            return;
        }

        console.log("updating");

        exec('git pull', {
                cwd: "/opt/iot_gateway/"
            },
            function (error, stdout, stderr) {
                if (error) {
                    console.error("Error while git pull");
                    console.error(JSON.stringify(error));
                } else {
                    console.log("git pulled, now rebooting");
                    rebootIfRaspberry();
                }
            });
    }

    function handleStatus(thingName, stat, clientToken, stateObject) {
        winston.info('handleStatus: ' + JSON.stringify(stateObject));
    }

    function handleDelta(thingName, stateObject) {
        winston.info('handleDelta: ' + JSON.stringify(stateObject));

        var reportObject = {
            state: {
                reported: {
                    r1: 0,
                    r2: 0
                }
            }
        };

        var state = stateObject.state;

        if (state.r1 != undefined) {
            winston.info('Writing pin ' + pinNumberForRelay(1) + ' value: ' + state.r1);
            gpio.writePin(pinNumberForRelay(1), state.r1)
            reportObject.state.reported.r1 = state.r1;
            pinStatus.r1 = state.r1;
        }
        if (state.r2 != undefined) {
            winston.info('Writing pin ' + pinNumberForRelay(2) + ' value: ' + state.r2);
            gpio.writePin(pinNumberForRelay(2), state.r2)
            reportObject.state.reported.r2 = state.r2;
            pinStatus.r2 = state.r2;
        }

        winston.info('Reporting object: ' + JSON.stringify(reportObject));
        genericOperation(thingName, 'update', reportObject);
    }

    function handleTimeout(thingName, clientToken) {
        var expectedClientToken = stack.pop();

        winston.info("handleTimeout called")

        if (expectedClientToken === clientToken) {
            winston.info('timeout on: ' + thingName);
        } else {
            winston.info('(timeout) client token mismtach on: ' + thingName);
        }

    }

    function handleCommand(command) {

        var commandObj;

        try {
           commandObj = JSON.parse(command);
        } catch(err) {
            winston.info("Command parsing error: " + command);
            return;
        }

        winston.info("handleCommand: " + JSON.stringify(commandObj));

        var relay = commandObj.r;
        var duration = commandObj.t;
        var relayValue = commandObj.rv;


        winston.info('Trigger relay ' + relay + ' with duration: ' + duration);

        if(relay=='1') {

            if(relayValue!=null) {
                gpio.writePin(pinNumberForRelay(1), relayValue)
                pinStatus.r1 = relayValue;
            } else {
                // Trigger command t1 received
                gpio.writePin(pinNumberForRelay(1), pinStatus.r1 ? 0 : 1)
                // If there is duration defined we will switch this relay back to its original state.
                if(duration != null) {
                    setTimeout(function () {
                        winston.info('Set relay 1 back to value: ' + pinStatus.r1);
                        gpio.writePin(pinNumberForRelay(1), pinStatus.r1)
                    }, duration);
                } else {
                    // No duration defined just switch it and leave and and save state.
                    pinStatus.r1 = pinStatus.r1 ? 0 : 1;
                }
            }
        } else if (relay == '2') {

            if(relayValue!=null) {
                gpio.writePin(pinNumberForRelay(2), relayValue)
                pinStatus.r2 = relayValue;
            } else {

                // Trigger command t2 received
                gpio.writePin(pinNumberForRelay(2), pinStatus.r2 ? 0 : 1)
                // If there is duration defined we will switch this relay back to its original state.
                if (duration != null) {
                    setTimeout(function () {
                        winston.info('Set relay 2 back to value: ' + pinStatus.r2);
                        gpio.writePin(pinNumberForRelay(2), pinStatus.r2)
                    }, duration);
                } else {
                    // No duration defined just switch it and leave and and save state.
                    pinStatus.r2 = pinStatus.r2 ? 0 : 1;
                }
            }
        }
    }

    function handlePing(data) {
        winston.info("handlePing: " + JSON.stringify(data));

        thingShadows.publish("pingreply/" + thingName, JSON.stringify({"ping":true}));
    }

    function handleUpdate(data) {

    }

    thingShadows.on('connect', function () {
        winston.info('connected to AWS IoT');
        reconnectTryCount = 0;
    });

    thingShadows.on('close', function () {
        winston.info('close');
        if (thingName != undefined)
            thingShadows.unregister(thingName);
    });

    thingShadows.on('reconnect', function () {
        winston.info('reconnect');
        reconnectTryCount++;
        if(reconnectTryCount == 10) {
            reboot();
        }
    });

    thingShadows.on('offline', function () {
        //
        // If any timeout is currently pending, cancel it.
        //
        if (currentTimeout !== null) {
            clearTimeout(currentTimeout);
            currentTimeout = null;
        }
        //
        // If any operation is currently underway, cancel it.
        //
        while (stack.length) {
            stack.pop();
        }
        winston.info('offline');
    });

    thingShadows.on('error', function (error) {
        winston.info('error', error);
    });

    thingShadows.on('message', function (topic, payload) {

        if(topic.startsWith('command')) {
            handleCommand(payload.toString());
        } else if(topic.startsWith('ping')) {
            handlePing(payload.toString());
        } else if(topic.startsWith('swupdate')){
          updateSW();
        } else {
            winston.info('Unidentified message received on topic: ', topic, " payload: " + payload.toString());
        }
    });

    thingShadows.on('status', function (thingName, stat, clientToken, stateObject) {
        //winston.info("on status: stat: " + stat);
        handleStatus(thingName, stat, clientToken, stateObject);
    });

    thingShadows.on('delta', function (thingName, stateObject) {
        handleDelta(thingName, stateObject);
    });

    thingShadows.on('timeout', function (thingName, clientToken) {
        handleTimeout(thingName, clientToken);
    });

}

module.exports = cmdLineProcess;

if (require.main === module) {
    cmdLineProcess('connect to the AWS IoT service and demonstrate thing shadow APIs, test modes 1-2',
        process.argv.slice(2), processTest);
}
