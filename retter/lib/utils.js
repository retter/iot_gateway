/**
 * Created by baranbaygan on 03/05/16.
 */

var self = {};

self.tryParseJson = function(str) {
    try {
        return JSON.parse(str);
    } catch (e) {
        return null;
    }
}

module.exports = self;