var Q = require('q');
var exec = require('exec');


module.exports = function () {

    var self = this;
    self.pinValues = {};


    self.setupDigitalInputPin = function (pin) {
        var defer = Q.defer();

        runExec('echo "' + pin + '" > /sys/class/gpio/export')
            .then(function () {
                console.log('export pin ' + pin + ' success');
                return runExec('chmod 777 /sys/class/gpio/gpio' + pin + '/direction');
            })
            // .then(function () {
            //     console.log('chmod pin ' + pin + ' success');
            //     return runExec('echo "high" > /sys/class/gpio/gpio' + pin + '/direction')
            // })
            .then(function () {
                return runExec('echo in > /sys/class/gpio/gpio' + pin + '/direction')
            })
            .then(function () {
                console.log('echo direction pin ' + pin + ' success');

                defer.resolve(true);
            })
            .fail(function () {
                console.log('failed');

                defer.reject(false);
            });

        return defer.promise;
    }

    self.listenToInputPin = function (pin, callback) {

        setInterval(function () {

            var cmdStr = 'cat /sys/class/gpio/gpio' + pin + '/value'
            exec(cmdStr, function (err, out, code) {

                var val = null;

                if (err instanceof Error) {
                    val = null;

                } else {
                    val = out;
                }

                if(self.pinValues[pin] != val) {
                    self.pinValues[pin] = val;
                    callback(val);
                }

            });

        }, 250);

    }

    self.setupPin = function (pin) {

        // echo "23" > /sys/class/gpio/export
        // chmod 777 /sys/class/gpio/gpio23/direction
        // echo "out" > /sys/class/gpio/gpio23/direction

        var defer = Q.defer();

        runExec('echo "' + pin + '" > /sys/class/gpio/export')
            .then(function () {
                console.log('export pin ' + pin + ' success');
                return runExec('chmod 777 /sys/class/gpio/gpio' + pin + '/direction');
            })
            .then(function () {
                console.log('chmod pin ' + pin + ' success');
                return runExec('echo "high" > /sys/class/gpio/gpio' + pin + '/direction')
            })
            .then(function () {
                console.log('echo direction pin ' + pin + ' success');

                defer.resolve(true);
            })
            .fail(function () {
                console.log('failed');

                defer.reject(false);
            });

        return defer.promise;

    }


    self.writePin = function (pin, value) {
        return runExec('echo "' + value + '" > /sys/class/gpio/gpio' + pin + '/value')
    }


    // Private methods
    function runExec(cmdStr) {
        var defer = Q.defer();

        exec(cmdStr, function (err, out, code) {
            if (err instanceof Error) {
                defer.reject(err);
                return;
            }
            defer.resolve(true);
        });

        return defer.promise;
    }

}
