/**
 * Created by baranbaygan on 13/06/16.
 */



const fs = require('fs');
const exec = require('child_process').exec;

const configPath = '/boot/network.config';
// const configPath = '/Users/baranbaygan/Desktop/network.config';
const wpa_supplicant_path = "/etc/wpa_supplicant/wpa_supplicant.conf";

function checkConfig() {

    fs.readFile(configPath, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        } else {
            // Network config file found. Read it. Update wpa_supplicant delete config file then reboot

            console.log("data: " + data);

            var lines = data.split('\n');

            if (lines.length > 1) {
                // Valid format

                var ssid = lines[0];
                var password = lines[1];

                console.log("SSID: " + ssid);
                console.log("Password: " + password);

                // Delete config file
                fs.unlinkSync(configPath);

                exec('sudo wpa_passphrase ' + ssid + " " + password, function (err, out, code) {


                    if(!err) {

                        console.log("wpa_passphrase result: " + out);

                        fs.writeFile(wpa_supplicant_path, out, function(err){

                            if(!err) {
                                // wpa_supplicant file changed. Reboot now.
                                exec('sudo reboot');
                            }

                        });

                    }


                });
            }


        }
        console.log(data);
    });

}

module.exports = checkConfig;