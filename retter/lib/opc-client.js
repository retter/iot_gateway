var net = require('net')
var Parser = require('binary-parser').Parser;

//node.js deps
const events = require('events');
const inherits = require('util').inherits;

var opcua = require("node-opcua");
var async = require("async");

//begin module

function OpcClient(opcSettings) {


    //
    // Force instantiation using the 'new' operator; this will cause inherited
    // constructors (e.g. the 'events' class) to be called.
    //
    if (!(this instanceof OpcClient)) {
        return new OpcClient(opcSettings);
    }


    var self = this;

    self.tagNameDataTypes = {};

    self.opcSettings = opcSettings;

    if (!self.opcSettings.opcPath) self.opcSettings.opcPath = "KEPServerEX";
    if (!self.opcSettings.opcRootFolder) self.opcSettings.opcRootFolder = "RootFolder";
    if (!self.opcSettings.opcSubFolder) self.opcSettings.opcSubFolder = "ObjectsFolder";

    var tags = self.opcSettings.tags;


    var client = new opcua.OPCUAClient();

    var endpointUrl = "opc.tcp://" + opcSettings.ipAddress + ":" + opcSettings.port + "/" + opcSettings.opcPath;

    var the_session = null;

    self.writeValue = function (opcTagName, value) {
        // console.log("Write value: " + nodeId + " " + value);
        if (!the_session) return;
        var nId = "ns=2;s=" + opcTagName;

        try {
            the_session.writeSingleNode(nId, {
                value: value,
                dataType: dataTypeForValue(opcTagName, value)
            }, function(){
                console.log("called back");
            });
        } catch(err) {
            console.log("err: " + err);
        }
    };

    function dataTypeForValue(opcTagName, value) {

        if(self.tagNameDataTypes[opcTagName]) {
            console.log("returning data type: " + self.tagNameDataTypes[opcTagName].key);
            return self.tagNameDataTypes[opcTagName].key;
        }

        switch(typeof value) {
            case "boolean": return "Boolean";
            case "number": return "Int32";
            case "string": return "String";
        }
    }

    async.series([


            // step 1 : connect to
            function (callback) {

                console.log("Connecting");
                client.connect(endpointUrl, function (err) {

                    if (err) {
                        console.log(" cannot connect to endpoint :", endpointUrl);
                    } else {
                        console.log("connected !");
                    }
                    callback(err);

                });
            },
            // step 2 : createSession
            function (callback) {
                client.createSession(function (err, session) {
                    if (!err) {
                        the_session = session;
                    }
                    callback(err);
                });

            },
            // step 3 : browse
            function (callback) {

                the_session.browse([self.opcSettings.opcRootFolder, self.opcSettings.opcSubFolder], function (err, browse_result, diagnostics) {
                    if (!err) {
                        browse_result[0].references.forEach(function (reference) {
                            console.log(reference.browseName);
                        });
                    }
                    callback(err);
                });
            },
            // step 4 : read a variable
            function (callback) {

                // setInterval(function(){
                //     the_session.readVariableValue("ns=2;s=Promocup.Device1.Machine4_ProductCount", function(err,dataValue) {
                //         if (!err) {
                //             console.log(" temperature = " , dataValue.toString());
                //         }
                //
                //     })
                // }, 500);

                callback(null);
            },

            // step 5: install a subscription and monitored item
            //
            // -----------------------------------------
            // create subscription
            function (callback) {

                the_subscription = new opcua.ClientSubscription(the_session, {
                    requestedPublishingInterval: 1000,
                    requestedLifetimeCount: 10,
                    requestedMaxKeepAliveCount: 2,
                    maxNotificationsPerPublish: 10,
                    publishingEnabled: true,
                    priority: 10
                });
                the_subscription.on("started", function () {
                    console.log("subscription started for 2 seconds - subscriptionId=", the_subscription.subscriptionId);
                }).on("keepalive", function () {
                    console.log("keepalive");
                }).on("terminated", function () {
                    callback();
                });

                // setTimeout(function(){
                //     the_subscription.terminate();
                // },10000);

                // install monitored item
                //

                tags.forEach(function (tag, i) {



                    // var tag = tags[i];
                    var monitoredItem = the_subscription.monitor({
                            //nodeId: opcua.resolveNodeId("ns=2;s=Promocup.Device1.Machine4_ProductCount"),
                            nodeId: opcua.resolveNodeId("ns=2;s=" + tag.opcTagName),
                            attributeId: 13
                            //, dataEncoding: { namespaceIndex: 0, name:null }
                        },
                        {
                            samplingInterval: 100,
                            discardOldest: true,
                            queueSize: 10
                        });

                    the_subscription.on("item_added", function (monitoredItem) {

                    });
                    //xx monitoredItem.on("initialized",function(){ });
                    //xx monitoredItem.on("terminated",function(value){ });


                    monitoredItem.on("changed", function (value) {
                        var it = monitoredItem;
                        // console.log(it.itemToMonitor.nodeId.value + " : " + value.value.toString());

                        self.tagNameDataTypes[tag.opcTagName] = value.value.dataType;

                        self.emit("valueChanged", {
                            opcTagName: tag.opcTagName,
                            tagName: tag.tag,
                            value: value.value.value,
                            dataType: value.value.dataType,
                            plcName: self.opcSettings.plcName
                        });
                    });

                });


            },

            // ------------------------------------------------
            // closing session
            //
            function (callback) {
                console.log(" closing session");
                the_session.close(function (err) {
                    console.log(" session closed");
                    callback();
                });
            },

        ],
        function (err) {
            if (err) {
                console.log(" failure ", err);
            } else {
                console.log("done!")
            }
            client.disconnect(function () {
            });
        });


    events.EventEmitter.call(this);
}

//
// Allow instances to listen in on events that we produce for them
//
inherits(OpcClient, events.EventEmitter);

module.exports = OpcClient;
