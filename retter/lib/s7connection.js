var net = require('net')
var Parser = require('binary-parser').Parser;

//node.js deps
const events = require('events');
const inherits = require('util').inherits;
const nodes7 = require('nodes7');

var plcCommErrorTimerId = undefined;
var plcCommErrorTimeout = 60000;
var plcCommErrorOccured = false;

//begin module

const plcReconnectionTimeout = 5000;

function S7Connection(deviceSettings) {


    //
    // Force instantiation using the 'new' operator; this will cause inherited
    // constructors (e.g. the 'events' class) to be called.
    //
    if (!(this instanceof S7Connection)) {
        return new S7Connection(deviceSettings);
    }

    var self = this;

    self.plcReadFailedMaxCount = 3;

    self.deviceSettings = deviceSettings;
    self.conn = null;
    self.values = {};



    function connectPlc() {

        console.log("Connecting plc with address: " + self.deviceSettings.ipAddress);

        self.conn = new nodes7;

        console.log("connecting to plc: " + self.deviceSettings.ipAddress);
        self.conn.initiateConnection({
            port: 102,
            host: self.deviceSettings.ipAddress,
            rack: 0,
            slot: 1
        }, function (err) {
            // PLC Connected callback
            if (!err) {
                console.log("PLC Connected");

                self.conn.setTranslationCB(function (tag) {
                    for (var i in self.deviceSettings.tags) {
                        var tagObj = self.deviceSettings.tags[i];
                        if (tagObj.tag == tag) {
                            if (tagObj.address === undefined) {
                                console.error("Invalid tag: " + JSON.stringify(tagObj));
                                return "";
                            }
                            console.log("translation: " + tagObj.address + " for tag: " + tag);
                            return tagObj.address;
                        }
                    }
                    console.log("Returning EMPTY for tag: " + tag);
                    return "";
                });  // This sets the "translation" to allow us to work with object names

                // Now let's add all tags to our connection
                self.deviceSettings.tags.forEach(function (el, index) {
                    console.log("Adding item: " + el.tag);
                    self.conn.addItems(el.tag);
                });
            } else {
                console.error("PLC Connection failed on s7connection.connectPlc");
                setTimeout(function () {
                    connectPlc();
                }, plcReconnectionTimeout);
            }
        });
    }



    function readPlcTagValues() {

        self.conn.readAllItems(function (anythingBad, values) {
            //console.log("readAllItems callback");
            if (anythingBad) {
                console.log("PLC readAllItems failed: self.plcReadFailedMaxCount: " + self.plcReadFailedMaxCount);

                self.plcReadFailedMaxCount--;

                if(self.plcReadFailedMaxCount <= 0) {
                    self.emit("plcReadFailedManyTimes");
                }
                // Reconnect this plc in 1 seconds
                setTimeout(function () {
                    connectPlc();
                }, plcReconnectionTimeout);
            } else {

                // Values are read successfully.
                for (var prop in values) {
                    // console.log("prop: " + prop + " value: " + values[prop]);
                    if (self.values[prop] !== values[prop]) {
                        self.emit("valueChanged", {
                            tagName: prop,
                            tagValue: values[prop]
                        });
                        self.values[prop] = values[prop];
                    }
                }
            }
        });
    }

    // self.writeQueue = [];
    // setInterval(function(){
    //     if(self.writeQueue.length == 0) return;
    //     if(self.isWriting) return;
    //
    //     try {
    //         var tagNames = [];
    //         var tagValues = [];
    //         self.writeQueue.forEach(function(el, i){
    //             tagNames.push(el.tagName);
    //             tagValues.push(el.tagValue);
    //         });
    //
    //         // Clear queue
    //         self.writeQueue = [];
    //
    //         console.log("tagNames: " + JSON.stringify(tagNames));
    //         console.log("tagValues: " + JSON.stringify(tagValues));
    //
    //         self.isWriting = true;
    //
    //         self.conn.writeItems(tagNames, tagValues, function (err) {
    //             if(err) console.log("s7connection write error: " + JSON.stringify(err));
    //             self.isWriting = false;
    //         });
    //
    //     } catch(err) {
    //         console.error("Cannot write value @ s7connection value: " + tagValue + " tagName: " + tagName);
    //         console.error(err);
    //     }
    //
    // }, 300);
    //
    // self.write = function (tagName, tagValue) {
    //     self.writeQueue.push({
    //         tagName: tagName,
    //         tagValue: tagValue
    //     });
    // }

    self.writeQueue = [];
    self.write = function (tagName, tagValue) {
        // If already writing just add to queue
        if(self.isWriting) {
            self.writeQueue.push({
                tagName: tagName,
                tagValue: tagValue
            });
        } else {
            // if not writing just write.
            self.isWriting = true;
            self.conn.writeItems(tagName, tagValue, function (err) {
                if (err) console.log("s7connection write error: " + JSON.stringify(err));
                self.isWriting = false;

                // We are done writing chec queue. If there is any more items to write call this function again.
                if(self.writeQueue.length > 0) {
                    var firstItem = self.writeQueue[0];
                    self.writeQueue.splice(0, 1);
                    self.write(firstItem.tagName, firstItem.tagValue);
                }
            });
        }
    }

    connectPlc();

    // Start reading values if we have any plc or not.
    setInterval(readPlcTagValues, 1000);


    events.EventEmitter.call(this);
}

//
// Allow instances to listen in on events that we produce for them
//
inherits(S7Connection, events.EventEmitter);

module.exports = S7Connection;
