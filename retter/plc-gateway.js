/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

//node.js deps

//npm deps

//app deps
const thingShadow = require('..').thingShadow;
const cmdLineProcess = require('./lib/cmdline');
const OpcClient = require('./lib/opc-client');
const winston = require('winston');
const exec = require('child_process').exec;
const S7connection = require('./lib/s7connection');

const networkConfigUpdater = require("./lib/network-config-updater");

var net = require('net');
var request = require('request');


var HOST = "192.168.1.42";
var PORT = 5002;

var opcClient;

// scada clients
var clients = [];

var plcTagValues = {};

var deviceList = [];

var doneReading = false;
var doneWriting = false;
var plcReadIntervalId = 0;

var namespace = "namespace";

var thingShadows;

var plcConnection;

var plcConfig;

var mqttMessageQueue = [];
const maxQueueSize = 100; // If this size is surpassed than queue will be send to mqtt and cleared.
const tagMqttPushInterval = 3000;

//begin module

// Configura logging
winston.add(winston.transports.File, {
    filename: '/var/log/plc-gateway.log',
    maxsize: 1024 * 1024 * 20, // MAX log file size is 20MB - Logs will be transferred to CloudWatch anyways
    maxFiles: 1
});
console.log = winston.info;
console.error = winston.error;

var networkConfigUtil = networkConfigUpdater();


function processTest(args) {

    console.log(JSON.stringify(args));

    process.on('uncaughtException', function (err) {
        console.error('Caught exception: ' + err);
        rebootIfRaspberry();
    });

    function rebootIfRaspberry() {
        console.log("reboot called");
        if (process.platform.indexOf("darwin") == -1) {
            console.log("Rebooting");
            exec('sudo reboot');
        }
    }

    function update() {

        // Return if macos
        if (process.platform.indexOf("darwin") != -1) return;

        console.log("updating");

        exec('sudo git pull', {
                cwd: "/opt/iot_gateway/"
            },
            function (error, stdout, stderr) {
                if (error) {
                    console.error("Error while git pull");
                    console.error(JSON.stringify(error));
                } else {
                    console.log("git pulled, now rebooting");
                    rebootIfRaspberry();
                }

            });
    }

    plcConfig = args.plcConfig;

// Fetch opc config
    request({
        url: "https://p3aflf1qza.execute-api.eu-west-1.amazonaws.com/prod/taglist",
        method: 'POST',
        body: JSON.stringify(plcConfig)
    }, function (err, resp, body) {
        if (!err) {
            console.log("Tag list fetched");


            var deviceData = JSON.parse(body).data;
            for (var deviceName in deviceData) {
                deviceList.push(deviceData[deviceName]);
            }

            startPLCRelatedConnections(deviceList);

            //
            // Instantiate the thing shadow class.
            //
            thingShadows = thingShadow({
                keyPath: args.privateKey,
                certPath: args.clientCert,
                caPath: args.caCert,
                clientId: args.clientId,
                region: args.region,
                baseReconnectTimeMs: args.baseReconnectTimeMs,
                keepalive: args.keepAlive,
                protocol: args.Protocol,
                port: args.Port,
                host: args.Host,
                debug: args.Debug
            });

            registerThingshadowEvents();

            startScadaServer();


        } else {
            console.log("Tag list fetch error");
        }

    });

    setInterval(function () {
        if (mqttMessageQueue.length == 0) return;
        pushQueueToMqtt();
    }, tagMqttPushInterval);

    function pushQueueToMqtt() {
        var topic = "plcUpdate/" + plcConfig.namespace;
        // publish messagequeue to mqtt
        thingShadows.publish(topic, JSON.stringify(mqttMessageQueue));
        // clear queue
        mqttMessageQueue = [];

    }

    function startScadaServer() {
        // Create the Scada socket server. This server will serve updated tag values to its clients.
        net.createServer(function (sock) {

            // We have a connection - a socket object is assigned to the connection automatically
            console.log('CONNECTED: ' + sock.remoteAddress + ':' + sock.remotePort);

            // Send all data currently stored to newly connected client
            sendCurrentDataToClient(sock);

            // Add this to our client list
            clients.push(sock);
            console.log("Client count: " + clients.length);


            // Add a 'data' event handler to this instance of socket
            sock.on('data', function (data) {
                console.log('DATA FROM OPC CLIENT ' + sock.remoteAddress + ': ' + data);

                try {
                    var obj = JSON.parse(data);
                    if (obj && obj.plcName && obj.tagName && obj.tagValue !== undefined) {
                        writeToDevice(obj.plcName, obj.tagName, obj.tagValue);
                        // writeToPlc(obj.plcName, obj.tagName, obj.tagValue);
                    }
                } catch (err) {
                    console.error("Invalid opc client request: " + JSON.stringify(err));
                }


            });

            // Add a 'close' event handler to this instance of socket
            sock.on('close', function (data) {
                for (var i in clients) {
                    if (clients[i] === sock) {
                        clients.splice(i, 1);
                        break;
                    }
                }
                console.log("Client count: " + clients.length);
                console.log('CLOSED: ' + sock.remoteAddress + ' ' + sock.remotePort);
            });

        }).listen({
            port: PORT
        }); //(PORT, HOST);
    }

    function startPLCRelatedConnections(deviceList) {
        console.log("TAG DATA: " + JSON.stringify(deviceList));

        deviceList.forEach(function (device) {
            if (device.deviceType == "opc") {
                device.opcClient = connectOpc(device);
            } else if (device.deviceType == "plc") {
                device.plcConnection = connectPlc(device);
            }
        });
    }

    function connectOpc(device) {
        opcClient = new OpcClient(device);

        opcClient.on("valueChanged", function (data) {
            console.log(JSON.stringify(data));

            tagValueChanged(data.plcName, data.tagName, data.value, data.dataType.toString());
        });
        return opcClient;
    }

    function connectPlc(device) {
        var connection = new S7connection(device);
        connection.on("valueChanged", function (data) {
            tagValueChanged(device.plcName, data.tagName, data.tagValue);
        });
        connection.on("plcReadFailedManyTimes", function () {
            rebootIfRaspberry();
        });
        return connection;
    }

    function tagValueChanged(plcName, tagName, tagValue, dataType) {
        console.log("tagValueChanged: plc: " + plcName + " tagName: " + tagName + " tagValue: " + tagValue + " dataType: " + dataType);

        var message = {
            plcName: plcName,
            tagValue: tagValue,
            tagName: tagName,
            dataType: dataType
        };

        for (var i = 0; i < mqttMessageQueue.length; i++) {
            var item = mqttMessageQueue[i];
            if (item.tagName == tagName) {
                mqttMessageQueue.splice(i, 1);
                break;
            }
        }
        mqttMessageQueue.push(message);

        if (mqttMessageQueue.length > maxQueueSize) {
            pushQueueToMqtt();
        }

        // Second publish to socket clients
        sendToScadaClients([message]);
    }


    function sendCurrentDataToClient(socket) {
        var dataToSend = [];
        for (var plcName in plcTagValues) {
            for (var tagName in plcTagValues[plcName]) {
                dataToSend.push({
                    plcName: plcName,
                    tagName: tagName,
                    tagValue: plcTagValues[plcName][tagName]
                });
            }
        }
        socket.write(JSON.stringify(dataToSend));
    }

    function sendToScadaClients(data) {
        for (var i in clients) {
            clients[i].write(JSON.stringify(data));
        }
    }

    function writeToPlc(plcDevice, tagName, tagValue) {
        plcDevice.plcConnection.write(tagName, tagValue);
    }

    function writeToOpc(opcDevice, tagName, tagValue) {
        opcDevice.tags.forEach(function (tag) {
            if (tag.tag == tagName) {
                opcDevice.opcClient.writeValue(tag.opcTagName, tagValue);
            }
        });
    }

    function writeToDevice(deviceName, tagName, tagValue) {
        deviceList.forEach(function (device) {
            if (device.plcName = deviceName) {
                if (device.deviceType == "opc") {
                    writeToOpc(device, tagName, tagValue);
                } else if (device.deviceType == "plc") {
                    writeToPlc(device, tagName, tagValue);
                }
            }
        });
    }

//
// Operation timeout in milliseconds
//
    const operationTimeout = 20000;

    var currentTimeout = null;

    var stack = [];

    var isAWSConnected = false;
    var awsNotConnectedMaxCount = 3;

    // Check every 60 seconds if aws is connected. If not connected 3 times reboot.
    setInterval(function () {
        if (isAWSConnected == false) {
            console.log("AWS Not connected, count: " + awsNotConnectedMaxCount);
            awsNotConnectedMaxCount--;
        }
        else {
            awsNotConnectedMaxCount = 3;
        }

        if(awsNotConnectedMaxCount < 0) {
            console.log("AWS Not connected for long. Rebooting.");
            rebootIfRaspberry();
        }
    }, 60000);


    function genericOperation(thingName, operation, state) {

        var clientToken = thingShadows[operation](thingName, state);

        if (clientToken === null) {
            //
            // The thing shadow operation can't be performed because another one
            // is pending; if no other operation is pending, reschedule it after an
            // interval which is greater than the thing shadow operation timeout.
            //
            if (currentTimeout !== null) {
                console.log('operation in progress, scheduling retry...');
                currentTimeout = setTimeout(
                    function () {
                        genericOperation(operation, state);
                    },
                    operationTimeout * 2);
            }
        } else {
            //
            // Save the client token so that we know when the operation completes.
            //
            stack.push(clientToken);
        }
    }


    function executeCommand(topic, payload) {
        try {
            var commandData = JSON.parse(payload);
            if (commandData.command === "reboot") {
                rebootIfRaspberry();
            } else if (commandData.command === "update") {
                update();
            }
        } catch (err) {
            console.error("Error while executing remote command: " + JSON.stringify(err));
        }
    }

    function registerThingshadowEvents() {

        thingShadows.on('connect', function () {
            console.log('connected to AWS IoT');
            isAWSConnected = true;
            deviceList.forEach(function (device, i) {
                var topicName = 'plcSet/' + plcConfig.namespace + "/" + device.plcName + "/+/update";
                console.log("Subscribing to topic: " + topicName);
                thingShadows.subscribe(topicName);
            });

            thingShadows.subscribe("remoteCommand/" + plcConfig.namespace);
        });

        thingShadows.on('close', function () {
            console.log('close');
            isAWSConnected = false;
            // if (thingName != undefined)
            //     thingShadows.unregister(thingName);
        });

        thingShadows.on('reconnect', function () {
            console.log('reconnect');
            isAWSConnected = false;
        });

        thingShadows.on('offline', function () {

            isAWSConnected = false;

            //
            // If any timeout is currently pending, cancel it.
            //
            if (currentTimeout !== null) {
                clearTimeout(currentTimeout);
                currentTimeout = null;
            }
            //
            // If any operation is currently underway, cancel it.
            //
            while (stack.length) {
                stack.pop();
            }
            console.log('offline');
        });

        // thingShadows.on('error', function (error) {
        //     console.log('error', error);
        // });


        thingShadows.on('message', function (topic, payload) {

            if (topic.indexOf("remoteCommand") != -1) {
                console.log("Command received in topic: " + topic + " payload: " + payload.toString());
                executeCommand(topic, payload);
                return;
            }

            console.log('message', topic, payload.toString());

            var updateObj = JSON.parse(payload);

            var parts = topic.split('/');
            if (parts.length < 5) return;

            var deviceName = parts[2];
            var tagName = parts[3];

            writeToDevice(deviceName, tagName, updateObj.tagValue);
            //writeToPlc(plcName, tagName, updateObj.tagValue);
        });

        // thingShadows.on('status', function (thingName, stat, clientToken, stateObject) {
        //     //console.log("on status: stat: " + stat);
        //     handleStatus(thingName, stat, clientToken, stateObject);
        // });

        // thingShadows.on('delta', function (thingName, stateObject) {
        //     handleDelta(thingName, stateObject);
        // });

        // thingShadows.on('timeout', function (thingName, clientToken) {
        //     handleTimeout(thingName, clientToken);
        // });
    }


// function startPlcCommErrorTimer() {
//     clearTimeout(plcCommErrorTimerId);
//
//     plcCommErrorTimerId = setTimeout(function () {
//         console.log("PLC Connection timeout error");
//         //thingShadows.publish("promocup/error/plc/" + args.thingName, "Oops! PLC Communication failure");
//         plcCommErrorOccured = true;
//     }, plcCommErrorTimeout);
// }
//
// startPlcCommErrorTimer();


    function reportedObjectWithData(reportedData) {
        return {
            'state': {
                'reported': reportedData
            }
        };
    }
}


module.exports = cmdLineProcess;

if (require.main === module) {
    cmdLineProcess('connect to the AWS IoT service and demonstrate thing shadow APIs, test modes 1-2',
        process.argv.slice(2), processTest);
}
